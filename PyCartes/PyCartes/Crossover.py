'''
    PyCartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import Individual
import Tree
import random
import operator

def Tournament(population, n, iterations):
    for x in range(iterations):
        picked = random.sample(population, n)
        for pick in picked:
            pick.CalculateFitness()
        best1 = max(picked, key = operator.attrgetter('Fitness'))
        picked.sort(key = operator.attrgetter('Fitness'))
        del picked [n-10:]
        population = [x for x in population if x not in picked]
        picked = random.sample(population, n)
        for pick in picked:
            pick.CalculateFitness()
        best2 = max(picked, key = operator.attrgetter('Fitness'))
        picked.sort(key = operator.attrgetter('Fitness'))
        del picked [n-10:]
        population = [x for x in population if x not in picked]
        population.extend(Crossover(best1.ReferencePoints, best1, best2))

    random.shuffle(population)


def Crossover(refPoints, ind1, ind2):
    list = []
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind1.FirstGene.Next2, ind1.FirstGene.Next1))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind2.FirstGene.Next2, ind2.FirstGene.Next1))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind1.FirstGene.Next1, ind2.FirstGene.Next1))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind2.FirstGene.Next1, ind1.FirstGene.Next1))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind1.FirstGene.Next2, ind2.FirstGene.Next2))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind2.FirstGene.Next2, ind1.FirstGene.Next2))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind1.FirstGene.Next1, ind2.FirstGene.Next2))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind2.FirstGene.Next1, ind1.FirstGene.Next2))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind1.FirstGene.Next2, ind2.FirstGene.Next1))
    list.append(Individual.Individual.GenerateFromGenes(refPoints, ind1.FirstGene.Value, ind2.FirstGene.Next2, ind1.FirstGene.Next1))
    return list

def Mutation(population, n):
    picked = random.sample(population, random.randint(1, n))
    for ind in picked:
        if random.randint(1, 2) == 1:
            population.append(Individual.Individual(ind.ReferencePoints, Tree.Operator(ind.FirstGene.Value, Tree.TreeElement.MakeRandomTree(), ind.FirstGene.Next2)))
        else:
            population.append(Individual.Individual(ind.ReferencePoints, Tree.Operator(ind.FirstGene.Value, ind.FirstGene.Next1, Tree.TreeElement.MakeRandomTree())))