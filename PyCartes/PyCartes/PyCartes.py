'''
    PyCartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from collections import namedtuple
import Genetics
import random

print("PyCartes AI - A Python port of the Descartes Genetic Algorithm")
print("Written by Francesco Pio Squillante for educational purpouse\n")

nPoints = input('How many points do you have? ')

Point = namedtuple("Points", "x y")

points = []

for i in range(int(nPoints)):
    print("\nPoint {0}".format(i))
    x = input('x = ')
    y = input('y = ')
    points.append(Point(float(x), float(y)))

pop = input('What is the start population? ')
iter = input('How many iteration for each generations? ')
gen = input('What is the maximum number of generations? ')
tourn = input('What is the number of individuals in a tournament? ')
seed = input('What is the random seed?')

random.seed(int(seed))

desInstance = Genetics.Genetics(int(pop), points, int(iter), int(gen), int(tourn))

best = desInstance.Evolve()

print("\n\nBest individual = {0}\tFitness = {1}".format(best.FirstGene.Build(), best.Fitness))
input()