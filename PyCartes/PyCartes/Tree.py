'''
    PyCartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import math
import random

functions = ["sin", "cos", "tan", "asin", "acos", "atan", "ln", "log10", "exp", "sqrt", "sinh", "cosh", "tanh", "abs"]
operators = ["+", "-", "*", "/", "^"]
variables = ["x", "x^2", "x^3", "x^4", "x^5"]

class TreeElement(object):

    @staticmethod
    def MakeRandomTree():
        rnd = random.randint(1, 4)
        if rnd == 1:
            return UnaryFunction(random.choice(functions), TreeElement.MakeRandomTree())
        elif rnd == 2:
            return Operator(random.choice(operators), TreeElement.MakeRandomTree(), TreeElement.MakeRandomTree())
        elif rnd == 3:
            return Variable(random.choice(variables))
        elif rnd == 4:
            return Number(str(random.uniform(-3, 3)))


class Variable(TreeElement):
    def __init__(self, value):
        self.Value = value
        
    def Evaluate(self, xvalue):
        if self.Value == "x":
            return xvalue
        elif self.Value == "x^2":
            return xvalue**2
        elif self.Value == "x^3":
            return xvalue**3
        elif self.Value == "x^4":
            return xvalue**4
        else:
            return xvalue**5

    def Build(self):
        return self.Value

class Number(TreeElement):
    def __init__(self, value):
        self.Value = value
        
    def Evaluate(self, xvalue):
        return float(self.Value)

    def Build(self):
        return self.Value

class UnaryFunction(TreeElement):

    def __init__(self, value, next):
        self.Value = value
        self.Next = next

    def Evaluate(self, xvalue):
        if self.Value == "sin":
            try:
                return math.sin(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "cos":
            try:
                return math.cos(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "tan":
            try:
                return math.tan(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "asin":
            try:
                return math.asin(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "acos":
            try:
                return math.acos(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        if self.Value == "atan":
            try:
                return math.atan(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "ln":
            try:
                return math.log(self.Next.Evaluate(xvalue), None)
            except ValueError:
                return float("NaN")
        elif self.Value == "log10":
            try:
                return math.log10(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "exp":
            return math.exp(self.Next.Evaluate(xvalue))
        elif self.Value == "sqrt":
            try:
                return math.sqrt(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        if self.Value == "sinh":
            try:
                return math.sinh(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "cosh":
            try:
                return math.cosh(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "tanh":
            try:
                return math.tanh(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")
        elif self.Value == "abs":
            try:
                return math.fabs(self.Next.Evaluate(xvalue))
            except ValueError:
                return float("NaN")

    def Build(self):
        return self.Value + "(" + self.Next.Build() + ")"

class Operator(TreeElement):

    def __init__(self, value, next1, next2):
        self.Value = value
        self.Next1 = next1
        self.Next2 = next2
        
    def Evaluate(self, xvalue):
        try:
            if self.Value == "+":
                return self.Next1.Evaluate(xvalue) + self.Next2.Evaluate(xvalue)
            elif self.Value == "-":
                return self.Next1.Evaluate(xvalue) - self.Next2.Evaluate(xvalue)
            elif self.Value == "*":
                return self.Next1.Evaluate(xvalue) * self.Next2.Evaluate(xvalue)
            elif self.Value == "/":
                try:
                    return self.Next1.Evaluate(xvalue) / self.Next2.Evaluate(xvalue)
                except ZeroDivisionError:
                    return float("Inf")
            elif self.Value == "^":
                try:
                    return self.Next1.Evaluate(xvalue) ** self.Next2.Evaluate(xvalue)
                except ZeroDivisionError:
                    return float("Inf")
        except OverflowError:
            return float("Inf")


    def Build(self):
        return self.Next1.Build() + self.Value + self.Next2.Build()
        