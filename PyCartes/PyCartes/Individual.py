'''
    PyCartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import Tree
import math
import random

class Individual(object):

    def __init__(self, points, firstGene = None):
        self.ReferencePoints = points
        if firstGene is None:
            self.FirstGene = Tree.Operator(random.choice(Tree.operators), Tree.TreeElement.MakeRandomTree(), Tree.TreeElement.MakeRandomTree())
        else:
            self.FirstGene = firstGene

    @classmethod
    def GenerateFromGenes(cls, points, operator, gene1, gene2):
        first = Tree.Operator(operator, gene1, gene2)
        return cls(points, first)


    def CalculateFitness(self):
        delta = 0
        for p in self.ReferencePoints:
            try:
                delta += (p.y - self.FirstGene.Evaluate(p.x))**2
            except (TypeError, OverflowError):
                delta = float("Inf")

        if type(delta) is not float:
            self.Fitness = float("NaN")
        else:
            if delta == 0:
                self.Fitness = float("Inf")
            elif math.isinf(delta):
                self.Fitness = 0
            else:
                self.Fitness = 1 / (delta / len(self.ReferencePoints))
