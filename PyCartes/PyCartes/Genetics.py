'''
    PyCartes AI - Genetic Algorithm to find the explicit equation of a list of point

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import Individual
import Crossover
import random
import operator

class Genetics(object):

    def __init__(self, startPopulation, refPoints, iterations, maxGenerations, tournamentNumber):
        self.Population = [Individual.Individual(refPoints) for n in range(startPopulation)]
        self.IterationsPerGeneration = iterations
        self.MaxGenerationsNumber = maxGenerations
        self.TournamentIndividuals = tournamentNumber

    def Evolve(self):
        for i in range(self.MaxGenerationsNumber):
            Crossover.Tournament(self.Population, self.TournamentIndividuals, self.IterationsPerGeneration)
            if random.random() < 0.1:
                Crossover.Mutation(self.Population, self.TournamentIndividuals)

        for individual in self.Population:
            individual.CalculateFitness()

        return max(self.Population, key=operator.attrgetter('Fitness'))
            

